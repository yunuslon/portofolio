# Portofolio

<h1><a href="https://gitlab.com/yunuslon/ecommerce-shop">Web Ecommers Shayna Customer</a></h1>

## Page Home
<img src="https://gitlab.com/yunuslon/ecommerce-shop/-/raw/master/img_prt/Home.png" alt="drawing" width="600"/>
<p>Page Home adalah tempat informasi barang yang akan di beli oleh customer, yang dimana data yang berada </p>
<p>dalam halaman ini di peroleh dari web CMS ecommerce admin</p>

## Page Product
<img src="https://gitlab.com/yunuslon/ecommerce-shop/-/raw/master/img_prt/product.png" alt="drawing" width="600"/>
<p>Page Product berfungsi untuk menampilkan detail product seperti foto foto dari product yang di pilih</p>

## Page checkout
<img src="https://gitlab.com/yunuslon/ecommerce-shop/-/raw/master/img_prt/checkout.png" alt="drawing" width="600"/>
<p>Page Checkout berfungsi untuk menampung data data product yang akan di beli oleh Customer, </p>
<p>disini juga tempat Customer memasukan biodata mereka untuk keperluan pembayaran dan pengiriman barang</p>
<p>Data data yang dimasukan customer akan otomatis akan terkirim ke web ecommerce admin untuk diproses lebih lanjut</p>

## Succes page
<img src="https://gitlab.com/yunuslon/ecommerce-shop/-/raw/master/img_prt/success.png" alt="drawing" width="600"/>

<h1><a href="https://gitlab.com/yunuslon/ecommerce-backend">Web CMS Shayna Admin </a></h1>

## Login
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/Login.png" alt="drawing" width="600"/> 
<p>Login berfungsi mengindentifikasi atau memfilter user yang berhak masuk dalam sistem</p>

## Home
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/Home.png" alt="drawing" width="600"/>
<p>Home berfungsi menampilkan informasi umum hasil penjualan,Seprti </p>
<ol>
  <li>Jumlah Penghasilan</li>
  <li>Jumlah penjualan sukses</li>
  <li>List customer yang melakukan transaksi</li>
  <li>Diagram informasi yang menampilkan tentang Jumlah transaksi panding,transaksi gagal dan transaksi sukses</li>
</ol>

## Daftar Barang
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/daftar-barang.png" alt="drawing" width="600"/>
<p>Fungsi daftar barang merupakan list informasi data dari barang yang tersedia untuk di jual </p>

## Tambah Barang
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/Tambah-barang.png" alt="drawing" width="600"/>
<p>Fungsi Tambah Barang adalah tempat admin menmabahkan barang yang akan di jual</p>

## Daftar Foto Barang
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/df-barang.png" alt="drawing" width="600"/>
<p>Daftar Foto Barang merupakan child dari tabel barang yang berfungsi untuk menampilkan daftar foto barang yang akan di tampilkan pada web Shayna customer</p>

## Tambah Foto Barang
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/tf-barang.png" alt="drawing" width="600"/>
<p>Tambah Foto Barang merupakan child dari tabel barang yang berfungsi untuk menambahkan foto barang oleh admin untuk  di tampilkan pada web Shayna customer</p>

## Daftar Transaksi Masuk
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/d-transaksi-masuk.png" alt="drawing" width="600"/>
<p>Daftar Transaksi Masuk berfungsi untuk menampilkan daftar transaksi yang telah dilakukan oleh customer di web Shayna customer</p>

## Detail Transaksi
<img src="https://gitlab.com/yunuslon/ecommerce-backend/-/raw/master/img_prt/detail_transaksi.png" alt="drawing" width="600"/>
<p>Detail Transaksi adalah tempat admin mengecek daftar transaksi yang akan di proses, Seprti untuk mengubah Status transaksi menjadi</p>
<ol>
   <li>Sukses</li>
   <li>Gagal</li>
   <li>Pending</li>
</ol>

